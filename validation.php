﻿<?php

/*$params = array(array("email" => "'/^[0-9A-Za-z]{1}[a-zA-Z0-9.]{0,}@\w+\.\w{1,4}(\.\w{1,4})?$/", "requierd" => true),
                array("password" => '.{8,}', "requierd" => true));


foreach ($params as $param){
    foreach ($param as $key => $value){
        if(!empty($_POST[$key])){

        }
    }
}*/

if (empty($_POST["email"])) {
    echo "Пожалуйста, укажите ваш email";
    die();
}

if (!preg_match ('/^[0-9A-Za-z]{1}[a-zA-Z0-9.]{0,}@\w+\.\w{1,4}(\.\w{1,4})?$/' ,$_POST["email"])) {
    echo "Ваш email некорректен!";
    die();
}

if (empty($_POST["password"])) {
    echo "Пожалуйста, введите ваш пароль";
    die();
}

if (!preg_match ('/^.{8,}$/' ,$_POST["password"])) {
    echo "Пароль минимум 8 символов";
    die();
}


if ($_POST["password"] != $_POST["check_password"]) {
    echo "Пароль не совпадает с проверочным";
    die();
}

if (!empty($_POST["phone"]) && !preg_match ('/^\+?[0-9]{11,16}$/' ,'+' + $_POST["phone"])){
    echo "Укажите, пожалуйста, ваш номер в международном формате";
    die();
}

if(!file_exists('users.xml')){
    $fp = fopen('users.xml', "w"); // ("r" - считывать "w" - создавать "a" - добовлять к тексту)
    fwrite($fp, "<?xml version=\"1.0\"?> \n<users> \n </users>");
    fclose($fp);
}


$xml = simplexml_load_file('users.xml');

$result = $xml->xpath('/users/user/email');


for($i = 0; $i < count($result); $i++){
    if($result[$i][0] == $_POST["email"]){
        echo "Данный емейл уже зарегестирован!";
        die();
    }
}

$book = $xml->addChild('user');
$book->addChild('email', $_POST["email"]);
$book->addChild('password', $_POST["password"]);
$book->addChild('phone', $_POST["phone"]);
$book->addChild('address', $_POST["address"]);
$xml->asXML('users.xml');


echo "Регистрация завверешна! Можете авторизироваться!";

?>